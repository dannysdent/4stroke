@section('title', 'About')

@include('header')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/img004.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span> <span>About us <i class="fa fa-chevron-right"></i></span></p>
            <h1 class="mb-0 bread">About Us</h1>
          </div>
        </div>
      </div>
    </section>
		
   	
    <section class="ftco-section ftco-no-pt ftco-no-pb bg-light">
    	<div class="container">
    		<div class="row d-flex no-gutters">
    			<div class="col-md-6 d-flex">
    				<div class="img img-video d-flex align-self-stretch align-items-center justify-content-center mb-4 mb-sm-0" style="background-image:url(images/003.jpg);">
    					<a href="#" class="icon-video popup-vimeo d-flex justify-content-center align-items-center">
    						<span class="fa fa-play"></span>
    					</a>
    				</div>
    			</div>
    			<div class="col-md-6 pl-md-5">
    				<div class="row justify-content-start py-5">
		          <div class="col-md-12 heading-section ftco-animate">
		          	<span class="subheading">Welcome to 4 Stroke Motors.</span>
		            <h2 class="mb-4">Over 5 years experience in car repair.</h2>
		            <p>Conveniently located, our facility boasts an extravagant 11,000 sq.ft of space set up with the latest in technologically advanced car care solutions. From an air flow controlled spray room (one of only four in Kenya) to vehicle lifts, engine re-locators  and stands, all the way down to high precision tools and equipment. We have the capacity to carry out private works, fleet maintenance, small commercial, heavy commercial among any other type of vehicle.
</p>
		            <div class="tabulation-2 mt-4">
									<ul class="nav nav-pills nav-fill d-md-flex d-block">
									  <li class="nav-item mb-md-0 mb-2">
									    <a class="nav-link active py-2" data-toggle="tab" href="#home1">Covenient</a>
									  </li>
									  <li class="nav-item px-lg-2 mb-md-0 mb-2">
									    <a class="nav-link py-2" data-toggle="tab" href="#home2">Affordable</a>
									  </li>
									  <li class="nav-item">
									    <a class="nav-link py-2 mb-md-0 mb-2" data-toggle="tab" href="#home3">Experienced</a>
									  </li>
									</ul>
									<div class="tab-content rounded mt-2">
									  <div class="tab-pane container p-0 active" id="home1">
									  	<p>We are located at Semco Industrial park, just ahead of Gateway Mall and directly opposite Kapa Oil. If you can't make it, our pick-up/drop-off service has you covered</p>
									  </div>
									  <div class="tab-pane container p-0 fade" id="home2">
									  	<p>There is a myth in Kenya that associates quality maintenance with 'bank breaking' prices; we are here to destroy that.At 4 Stroke Motors, you are assured of above-par quality at the most affordable rates. </p>
									  </div>
									  <div class="tab-pane container p-0 fade" id="home3">
									  	<p>Our team, not only qualified, has a combined experience of over 50 years assuring nothing but outstanding quality and efficiency for you and your vehicle.</p>
									  </div>
									</div>
								</div>
		          </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section testimony-section bg-light">
      <div class="container">
        <div class="row justify-content-center pb-5 mb-3">
          <div class="col-md-7 heading-section heading-section-white text-center ftco-animate">
          	<span class="subheading">Testimonies</span>
            <h2>Happy Clients &amp; Feedbacks</h2>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel ftco-owl">
              <div class="item">
                <div class="testimony-wrap py-4">
                	<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></span></div>
                  <div class="text">
                    <p class="mb-4">Its always good to find someone you trust. I feel this way about 4 Stroke experience. Things are easier these days, I really trust you guys.</p>
                    <div class="d-flex align-items-center">
                    	<div class="pl-3">
		                    <p class="name">Riksha</p>
		                    <span class="position">Pegeout 504</span>
		                  </div>
	                  </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap py-4">
                	<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></span></div>
                  <div class="text">
                    <p class="mb-4">Super friendly staff with consistent and regular vehicle updates...competitive pricing.</p>
                    <div class="d-flex align-items-center">
                    	<div class="pl-3">
		                    <p class="name">Kennedy</p>
                        <span class="position">Range Rover</span>
		                  </div>
	                  </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap py-4">
                	<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></span></div>
                  <div class="text">
                    <p class="mb-4">Very polite. They explain all the work they're going to do and have fair prices.</p>
                    <div class="d-flex align-items-center">
                    	<div class="pl-3">
		                    <p class="name">Vincent</p>
                        <span class="position">Toyota Wish</span>
		                  </div>
	                  </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap py-4">
                	<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></span></div>
                  <div class="text">
                    <p class="mb-4">Everything was great. I was very happy with the work and will definitely return for any other work.</p>
                    <div class="d-flex align-items-center">
                    	<div class="pl-3">
		                    <p class="name">Wilfred</p>
                        <span class="position">NIssan Bluebird</span>
		                  </div>
	                  </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap py-4">
                	<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></span></div>
                  <div class="text">
                    <p class="mb-4">Great experience. Got a new truck repair center!</p>
                    <div class="d-flex align-items-center">
                    	<div class="pl-3">
		                   <p class="name">Kevin</p>
                        <span class="position">Subaru Forester</span>
		                  </div>
	                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

@include('footer')