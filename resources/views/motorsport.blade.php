@section('title', 'Motorsport')

@include('header')

    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/img007.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span></span> <span>Motorsport</span></p>
            <h1 class="mb-0 bread">Motorsport</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate">
            <p>
              <img src="images/p7.jpg" alt="" class="img-fluid">
            </p>
            <h2 class="mb-3">For your car racing needs.</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eius mollitia suscipit, quisquam doloremque distinctio perferendis et doloribus unde architecto optio laboriosam porro adipisci sapiente officiis nemo accusamus ad praesentium? Esse minima nisi et. Dolore perferendis, enim praesentium omnis, iste doloremque quia officia optio deserunt molestiae voluptates soluta architecto tempora.</p>
            <p>Molestiae cupiditate inventore animi, maxime sapiente optio, illo est nemo veritatis repellat sunt doloribus nesciunt! Minima laborum magni reiciendis qui voluptate quisquam voluptatem soluta illo eum ullam incidunt rem assumenda eveniet eaque sequi deleniti tenetur dolore amet fugit perspiciatis ipsa, odit. Nesciunt dolor minima esse vero ut ea, repudiandae suscipit!</p>
          </div> <!-- .col-md-8 -->
          <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Services</h3>
                <li><a href="#">Oil Change <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Batteries &amp; Brake Check <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Tow Truck <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Tire Change <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Engine Repair<span class="fa fa-chevron-right"></span></a></li>
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
              <h3>Motorsport Offers</h3>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of5.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Complete car paint.</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh  60,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of3.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Electrical wiring</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 50,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of4.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Wheel alignment</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 90,000</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->

@include('footer')