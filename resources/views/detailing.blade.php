@section('title', 'Detailing')

@include('header')

    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/pics/pic18.JPG');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span></p>
            <h1 class="mb-0 bread">Detailing</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate">
            <p>
              <img src="images/dt.jpg" alt="" class="img-fluid">
            </p>
            <h2 class="mb-3">We detail your car for the perfect look.</h2>
            <p>Many people think car washing and car detailing are one and the same. But this is not the case. Detailed car washing goes beyond the normal car wash to make a vehicle look spotlessly clean both inside and outside. Unlike car washing, car detailing does not involve using an automated system to do the cleaning. Instead, it involves handwashing by very experienced detailers.</p>

			<p>Car detailing is divided into two components: Interior Detailing and Exterior Detailing.</p>

			<p><b>Exterior Car Detailing</b> – It involves vacuuming, restoring, and surpassing the original condition of the exterior constituents of a vehicle, such as tires, windows and wheels, among other visible components. Products include but are not limited to: polishes, wax, detergents and degreasers.</p>

			<p><b>Interior Car Detailing</b> – Interior detailing involves cleaning the inner parts of a vehicle. Some of the components found in the interior cabin include leather, plastics, vinyl, carbon fiber plastics and natural fibers. To clean the interior cabinet, different techniques such as steam-cleaning and vacuuming are used.</p>
          </div> <!-- .col-md-8 -->
          <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Services</h3>
                <li><a href="#">Exterior car detailing <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Interior car detailing <span class="fa fa-chevron-right"></span></a></li>
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
              <h3>Detailing offers</h3>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of5.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Complete car paint.</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh  60,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of3.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Electrical wiring</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 50,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of4.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Wheel alignment</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 90,000</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->

@include('footer')