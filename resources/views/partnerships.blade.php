@section('title', 'Partnerships')

@include('header')

    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/pics/pic7.JPG');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span></a></span> <span>Partnerships</span></p>
            <h1 class="mb-0 bread">Partnerships</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate">
            <p>
              <img src="images/pt.png" alt="" class="img-fluid">
            </p>
            <h2 class="mb-3">We have partnered with the folowing companies</h2>
            <p>Our self-driving strategy centers around partnership. By combining 4Stroke’s self-driving technology with partners’ state-of-the-art vehicles, sensors, and technology, we’ll get to the future faster.</p>
            <p>Since 2015, we’ve been working to bring safe, reliable self-driving technology to the 4Stroke network. We knew we couldn’t do it alone, which is why we continue partnering with world-class vehicle manufacturers to make our vision a reality.</p>
          </div> <!-- .col-md-8 -->
          <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Reviews</h3>
                <div class="py-4">
                  <div class="icon d-flex align-items-center justify-content-center"></div>
                  <div class="text">
                    <p class="mb-4">Really loved their car repair service.Will definitely bring my car again.</p>
                    <div class="d-flex align-items-center">
                      <div class="pl-3">
                        <p><b>Justine</b></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="py-4">
                  <div class="icon d-flex align-items-center justify-content-center"></div>
                  <div class="text">
                    <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <div class="d-flex align-items-center">
                      <div class="pl-3">
                        <p><b>Stanley</b></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
              <h3>General Offers</h3>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of5.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Complete car paint.</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh  60,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of3.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Electrical wiring</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 50,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of4.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Wheel alignment</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 90,000</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->

@include('footer')