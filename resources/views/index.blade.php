@section('title', 'Home')

@include('header')
    <div class="hero-wrap">
	    <div class="home-slider owl-carousel">
	      <div class="slider-item" style="background-image:url(images/img002.jpg);">
	      	<div class="overlay"></div>
	        <div class="container">
	          <div class="row no-gutters slider-text align-items-center justify-content-start">
		          <div class="col-md-6 ftco-animate">
		          	<div class="text w-100">
		          		<h2>Best car repair services</h2>
			            <h1 class="mb-4">Make your car last longer</h1>
			            <p><a href="/book" class="btn btn-primary">Book an appointment</a></p>
		            </div>
		          </div>
		        </div>
	        </div>
	      </div>

	      <div class="slider-item" style="background-image:url(images/img003.jpg);">
	      	<div class="overlay"></div>
	        <div class="container">
	          <div class="row no-gutters slider-text align-items-center justify-content-start">
		          <div class="col-md-6 ftco-animate">
		          	<div class="text w-100">
		          		<h2>We care about your car</h2>
			            <h1 class="mb-4">It's time to come repair your car</h1>
			            <p><a href="/book" class="btn btn-primary">Book an appointment</a></p>
		            </div>
		          </div>
		        </div>
	        </div>
	      </div>
	    </div>
	  </div>

       <style type="text/css">

/*********************/
/*     14. About     */
/*********************/
.basic-4 {
  padding-top: 7rem;
  padding-bottom: 4rem;
  text-align: center;
}

.basic-4 .team-member {
  max-width: 12.5rem;
  margin-right: auto;
  margin-bottom: 3.5rem;
  margin-left: auto;
  
}

/* Hover Animation */
.basic-4 .image-wrapper1 {
  overflow: hidden;
  margin-bottom: 1.5rem;
  border-radius: 50%;
  border-style: solid;
  border-color: red;
  border-width: thick;
}

.basic-4 .image-wrapper2 {
  overflow: hidden;
  margin-bottom: 1.5rem;
  border-radius: 50%;
  border-style: solid;
  border-color: blue;
  border-width: thick;
}

.basic-4 .image-wrapper3 {
  overflow: hidden;
  margin-bottom: 1.5rem;
  border-radius: 50%;
  border-style: solid;
  border-color: orange;
  border-width: thick;
}

.basic-4 .image-wrapper4 {
  overflow: hidden;
  margin-bottom: 1.5rem;
  border-radius: 50%;
  border-style: solid;
  border-color: pink;
  border-width: thick;
}

.basic-4 .image-wrapper5 {
  overflow: hidden;
  margin-bottom: 1.5rem;
  border-radius: 50%;
  border-style: solid;
  border-color: green;
  border-width: thick;
}
.basic-4 .image-wrapper6 {
  overflow: hidden;
  margin-bottom: 1.5rem;
  border-radius: 50%;
  border-style: solid;
  border-color: indigo;
  border-width: thick;
}

.basic-4 .image-wrapper img {
  margin: 0;
  transition: all 0.3s;
}

.basic-4 .image-wrapper:hover img {
  -moz-transform: scale(1.15);
  -webkit-transform: scale(1.15);
  transform: scale(1.15);
} 
/* end of hover animation */

.basic-4 .team-member .p-large {
  margin-bottom: 0.25rem;
  font-size: 1.125rem;
}

.basic-4 .team-member .job-title {
  margin-bottom: 0.375rem;
}


   
    </style>
    <!-- slider here -->

    <div id="about" class="basic-4">
        <div class="container">
            <div class="row">
            </div> <!-- end of row -->
            <div class="row">
                  <div class="col-md-4">
                    <!-- Team Member -->
                    <div class="team-member" href="saloon.html">
                        <div class="image-wrapper1">
                           <a href="/saloon"><img class="img-fluid" src="images/p4.jpg" alt="alternative"></a>
                        </div> <!-- end of image-wrapper -->
                        <div class="media-body pl-3">
                          <h5>Saloons, SUV'S & 4x4's</h5>
                          <p><a href="/saloon" class="btn-custom">Read more</a></p>
                        </div>
                    </div> <!-- end of team-member -->
                    <!-- end of team member -->
                  </div>

                  <div class="col-md-4">
                    <!-- Team Member -->
                    <div class="team-member">
                        <div class="image-wrapper2">
                            <a href="/body-paint"><img class="img-fluid" src="images/of2.jpeg" alt="alternative"></a>
                        </div> <!-- end of image-wrapper -->
                        <div class="media-body">
                          <h5 class="heading">Panel Beating & Spray Painting</h5>
                          <p><a href="/body-paint" class="btn-custom">Read more</a></p>
                        </div>
                    </div> <!-- end of team-member -->
                    <!-- end of team member -->
                  </div>

                  <div class="col-md-4">
                    <!-- Team Member -->
                    <div class="team-member">
                        <div class="image-wrapper3">
                            <a href="/truck-fleet"><img class="img-fluid" src="images/pics/pic1.JPG" alt="alternative"></a>
                        </div> <!-- end of image-wrapper -->
                        <div class="media-body">
                          <h5 class="heading">Truck & Fleet Management</h5>
                          <p><a href="/truck-fleet" class="btn-custom">Read more</a></p>
                        </div>
                    </div> <!-- end of team-member -->
                    <!-- end of team member -->
                  </div>

                  <div class="col-md-4">
                    <!-- Team Member -->
                    <div class="team-member">
                        <div class="image-wrapper4">
                            <a href="/partnerships"><img class="img-fluid" src="images/p1.jpg" alt="alternative"></a>
                        </div> <!-- end of image-wrapper -->
                        <div class="media-body">
                          <h5 class="heading">Partnerships</h5>
                          <p><a href="/partnerships" class="btn-custom">Read more</a></p>
                        </div>
                    </div> <!-- end of team-member -->
                    <!-- end of team member -->
                  </div>

                  <div class="col-md-4">
                    <!-- Team Member -->
                    <div class="team-member">
                        <div class="image-wrapper5">
                            <a href="/motorsport"><img class="img-fluid" src="images/p7-2.jpg" alt="alternative"></a>
                        </div> <!-- end of image-wrapper -->
                        <div class="media-body">
                          <h5 class="heading">Motorsport</h5>
                          <p><a href="/motorsport" class="btn-custom">Read more</a></p>
                        </div>
                    </div> <!-- end of team-member -->
                    <!-- end of team member -->
                  </div>

                  <div class="col-md-4">
                    <!-- Team Member -->
                    <div class="team-member">
                        <div class="image-wrapper6">
                            <a href="/detailing"><img class="img-fluid" src="images/pics/pic-18.JPG" alt="alternative"></a>
                        </div> <!-- end of image-wrapper -->
                        <div class="media-body">
                          <h5 class="heading">Interior & Exterior Detailing</h5>
                          <p><a href="/detailing" class="btn-custom">Read more</a></p>
                        </div>
                    </div> <!-- end of team-member -->
                    <!-- end of team member -->
                  </div>


               
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-4 -->
    <!-- end of about -->

        <section class="ftco-counter" id="section-counter">
    	<div class="container">
				<div class="row">
          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="10">0</strong>
              </div>
              <div class="text">
              	<span>Years of Experienced</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="850">0</strong>
              </div>
              <div class="text">
              	<span>Project completed</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="2304">0</strong>
              </div>
              <div class="text">
              	<span>Happy Customers</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="15">0</strong>
              </div>
              <div class="text">
              	<span>Award Winning</span>
              </div>
            </div>
          </div>
        </div>
    	</div>
    </section>

@include('footer')