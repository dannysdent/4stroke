@section('title', 'Truck & Fleet')

@include('header')

    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/pics/pic1.JPG');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span></span> <span>Truck & Fleet</span></p>
            <h1 class="mb-0 bread">Truck & Fleet</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate">
            <p>
              <img src="images/pics/pic10.JPG" alt="" class="img-fluid">
            </p>
            <h2 class="mb-3">Need a service for your truck or fleet?.</h2>
            <p>How you handle your fleet maintenance can have a major impact on your bottom line. Vehicle type, years in service and the industry you serve are only a few key factors that impact how your fleet trucks are maintained. At 4 Stroke, we know every truck is different, and that’s why we provide a comprehensive vehicle maintenance program to improve uptime, control costs and provide an unprecedented level of insight to help you make the best strategic decisions for your fleet.</p>
          </div> <!-- .col-md-8 -->
          <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Services</h3>
                <li><a href="#">Fleet maintenance<span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Fleet repair <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Comprehensive diagnostics <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Engine Repair<span class="fa fa-chevron-right"></span></a></li>
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
              <h3>Truck & Fleet Offers</h3>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of5.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Complete car paint.</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh  60,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of3.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Electrical wiring</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 50,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of4.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Wheel alignment</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 90,000</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->

@include('footer')