@section('title', 'Body & Paint')

@include('header')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/pics/pic15.JPG');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span></span> <span>Body & Paint</span></p>
            <h1 class="mb-0 bread">Body & Paint</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate">
            <p>
              <img src="images/p9.jpg" alt="" class="img-fluid">
            </p>
            <h2 class="mb-3">Get a paint job from the professionals.</h2>
            <p>At 4 Stroke, we have over 10 years of experience in auto-body repair work in Nairobi and carry out all types of body-work repairs from minor cosmetic scratches and dents to severe accident damage on all makes and models of cars of both saloon and heavy commercial vehicles.</p>

            <p>Our team has the knowledge, skills and the latest equipment available for top class panel beating work and re-spraying to return your car to its pre-accident condition. We offer a fast, reliable and professional service at competitive prices.</p>

            <p>Furthermore, if your car is immobile, we can collect it from anywhere in Nairobi using our recovery truck.</p>

            <p>We also carry out full re-sprays from just 49,990/- depending on the make and model of your vehicle.</p>
          </div> <!-- .col-md-8 -->
          <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Services</h3>
                <li><a href="#">Panel beating <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Re-spraying <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Minor cosmetic scratch repair <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Full respray <span class="fa fa-chevron-right"></span></a></li>
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
              <h3>Body & Paint Offers</h3>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of5.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Complete car paint.</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh  60,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of3.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Electrical wiring</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 50,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of4.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Wheel alignment</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 90,000</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->

@include('footer')