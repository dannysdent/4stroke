@section('title', 'Saloon | 4x4 | SUV')

@include('header')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/pics/pic11.JPG');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span></span> <span>Saloon , SUV & 4x4</span></p>
            <h1 class="mb-0 bread">Saloon , SUV & 4x4</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate">
          	<p>
              <img src="images/pics/pic3.JPG" alt="" class="img-fluid">
            </p>
            <h2 class="mb-3">We offer the best services for your saloon car.</h2>
            <p>Servicing your car is one of the most important things you can do to maintain its performance, safety and fuel efficiency. Regular service will pay for itself over the lifetime of the car and ensure you, your family’s and other road user’s safety.</p>

 

            <p>There are 2 types of services:</p>

 

            <p><b>Minor Service -</b> This is done every 5000 kms and involves an oil change, oil filter replacement and a thorough inspection of all brakes, fluids, filters, hoses, suspension parts and emissions.
            Charges from 4990/- Kshs.</p>

            <p><b>Major Service - </b>This is done every 10,000 kms and involves the replacement of the oil filter, air filter, fuel filter, spark plugs and oil. We will also check and replace if necessary the brake pads, brake linings, ball-joints, tie-rod ends and shocks. The gearbox oil, automatic transmission fluid (ATF), differential oil and other fluids will also be checked and topped up if necessary.
            Charges from 8,990/- Kshs.</p>
          </div> <!-- .col-md-8 -->
          <div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Services</h3>
                <li><a href="#">Vehicle Diagnostics <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Engine Overhauls and Replacement<span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Gearbox Overhauls and Replacement<span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Air Conditioning Service and Repair <span class="fa fa-chevron-right"></span></a></li>
                <li><a href="#">Electrical System Diagnostic and Repair<span class="fa fa-chevron-right"></span></a></li>
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
              <h3>Saloon Car Offers</h3>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of5.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Complete car paint.</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh  60,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of3.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Electrical wiring</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 50,000</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/of4.jpeg);"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">Wheel alignment</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>Ksh 90,000</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->

@include('footer')