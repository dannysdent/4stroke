@section('title', 'Book Appoitment')

@include('header')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/img006.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span> <span>Book Appointment <i class="fa fa-chevron-right"></i></span></p>
            <h1 class="mb-0 bread">Book Appointment</h1>
          </div>
        </div>
      </div>
    </section>
    <br>

    <section class="ftco-appointment ftco-section ftco-no-pt ftco-no-pb img" style="background-image: url(images/002.jpg);">
      <div class="overlay"></div>
      <style type="text/css">
        .imgo img{
          margin-right: 20px;
          margin-bottom: 20px;
          width: 150px;
          height: 150px;
        }
      </style>
      <div class="container">
        <div class="row d-md-flex justify-content-end">
          <div class="col-lg-6 half p-3 py-5 pl-lg-5 ftco-animate imgo heading-section heading-section-white">
            <div class="row">
              <img src="images/of3.jpeg">
              <br>
              <img src="images/of3.jpeg">
              <br>
              <img src="images/of3.jpeg">
            </div>

            <div class="row">
              <img src="images/of3.jpeg">
              <br>
              <img src="images/of3.jpeg">
              <br>
              <img src="images/of3.jpeg">
            </div>

            <div class="row">
              <img src="images/of3.jpeg">
              <br>
              <img src="images/of3.jpeg">
              <br>
              <img src="images/of3.jpeg">
            </div>

            
          </div>
            <div class="col-md-12 col-lg-6 half p-3 py-5 pl-lg-5 ftco-animate heading-section heading-section-white">
            <span class="subheading">Booking an Appointment</span>
            <h2 class="mb-4">Free Consultation</h2>
            <form action="#" class="appointment">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Your Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Vehicle number">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-wrap">
                      <div class="icon"><span class="fa fa-calendar"></span></div>
                      <input type="text" class="form-control appointment_date" placeholder="Date">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-wrap">
                      <div class="icon"><span class="fa fa-clock-o"></span></div>
                      <input type="text" class="form-control appointment_time" placeholder="Time">
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Specify service"></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="submit" value="Book Appointment" class="btn btn-dark py-3 px-4">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <br>

@include('footer')