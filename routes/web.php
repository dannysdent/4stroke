<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('body-paint', function () {
    return view('body-paint');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/motorsport', function () {
    return view('motorsport');
});
Route::get('/partnerships', function () {
    return view('partnerships');
});
Route::get('/saloon', function () {
    return view('saloon');
});
Route::get('/truck-fleet', function () {
    return view('truck-fleet');
});
Route::get('/projects', function () {
    return view('projects');
});
Route::get('/book', function () {
    return view('book');
});
Route::get('/detailing', function () {
    return view('detailing');
});